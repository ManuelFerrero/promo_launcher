<cfprocessingdirective suppresswhitespace="yes">
<cfheader name="Access-Control-Allow-Origin" value="*">
<cfoutput>
<cfif not isdefined("url.id")>
    <cfcontent type="text/plain"><cfoutput>ERROR: falta ID</cfoutput>
    <cfexit>
</cfif>
<cfsilent>
   	<cfset url.id=rereplace(url.id,"[^0-9]","","ALL")>
    <cfquery datasource="#dsn#" name="acciones">
        SELECT 
            `mxr_acciones_recover`.`cod_accion`,
            `mxr_acciones_recover`.`val_parametro`,
            `mxr_acciones_recover`.`val_accion`
        FROM
            `mxr_acciones_recover`
        WHERE
            `mxr_acciones_recover`.`cod_terminal` = <cfqueryparam cfsqltype="cf_sql_integer" value="#url.id#" maxlength="11">	 AND 
            `mca_enviada` = 'N' 
    </cfquery>

</cfsilent>
<cfset salida="">
<cfloop query="acciones">
    <cfset salida="#url.id####acciones.val_accion#">
    <cfif acciones.val_parametro NEQ "">
        <cfset parametro_accion="###acciones.val_parametro#">
        <cfelse>
            <cfset parametro_accion=""> 	
    </cfif>

    <cfif acciones.val_parametro NEQ "">
        <cfset salida="#salida####acciones.val_parametro#">  
    </cfif>
    <cfset salida="#salida##chr(13)#">
    <cfif not isdefined("noborrar")>
        <cfquery datasource="#dsn#">
            update mxr_acciones_recover 
            set mca_enviada='S',
            fec_ejecucion=now()
            where cod_accion = #acciones.cod_accion#
        </cfquery>
    </cfif>
</cfloop>
</cfoutput>
</cfprocessingdirective>
<cfcontent type="text/plain"><cfoutput>#salida#</cfoutput>

<!---  
Tabla

 
CREATE TABLE mxr_acciones_recover (
    cod_accion     int AUTO_INCREMENT NOT NULL,
    val_accion     varchar(100) NOT NULL,
    val_parametro  varchar(500),
    mca_enviada    enum ('S','N') DEFAULT 'N',
    fec_creacion   datetime,
    fec_ejecucion  datetime,
    cod_terminal   int NOT NULL,
    /* Keys */
    PRIMARY KEY (cod_accion)
  ) ENGINE = InnoDB;


  --->