unit leer_config;

interface

uses
  Classes, SysUtils, IniFiles, Forms, Windows;

const
  csIniCONFIGURACIONSection = 'CONFIGURACION';
  csIniMQTTSection = 'MQTT';
  csIniHTTPSection = 'HTTP';
  csIniWEB_SERVERSection = 'WEB_SERVER';
  csIniPROXYSection = 'PROXY';

  {Section: CONFIGURACION}
  csIniCONFIGURACIONINI_PLAYER = 'INI_PLAYER';
  csIniCONFIGURACIONGRUPO = 'GRUPO';
  csIniCONFIGURACIONreboot = 'reboot';
  csIniCONFIGURACIONintervalo_reboot = 'intervalo_reboot';
  csIniCONFIGURACIONintervalo_idioma = 'intervalo_idioma';
  csIniCONFIGURACIONtime_diff = 'time_diff';

  {Section: MQTT}
  csIniMQTTENABLED = 'ENABLED';
  csIniMQTTHOST = 'HOST';
  csIniMQTTPORT = 'PORT';
  csIniMQTTSSL = 'SSL';
  csIniMQTTTOPIC_PUB = 'TOPIC_PUB';
  csIniMQTTTOPIC_SUB = 'TOPIC_SUB';
  csIniMQTTUSER = 'USER';
  csIniMQTTPASS = 'PASS';
  csIniMQTTDEBUG = 'DEBUG';

  {Section: HTTP}
  csIniHTTPENABLED = 'ENABLED';
  csIniHTTPURL = 'URL';
  csIniHTTPINTERVAL = 'INTERVAL';

  {Section: WEB_SERVER}
  csIniWEB_SERVERenabled = 'enabled';
  csIniWEB_SERVERport = 'port';

  {Section: PROXY}
  csIniPROXYusar = 'usar';
  csIniPROXYHOST = 'HOST';
  csIniPROXYPORT = 'PORT';
  csIniPROXYUSER = 'USER';
  csIniPROXYPASS = 'PASS';

type
  TIniOptions = class(TObject)
  private
    {Section: CONFIGURACION}
    FCONFIGURACIONINI_PLAYER: string;
    FCONFIGURACIONGRUPO: Integer;
    FCONFIGURACIONreboot: Integer;
    FCONFIGURACIONintervalo_reboot: Integer;
    FCONFIGURACIONintervalo_idioma: Integer;
    FCONFIGURACIONtime_diff: Integer;

    {Section: MQTT}
    FMQTTENABLED: Integer;
    FMQTTHOST: string;
    FMQTTPORT: Integer;
    FMQTTSSL: Integer;
    FMQTTTOPIC_PUB: string;
    FMQTTTOPIC_SUB: string;
    FMQTTUSER: string;
    FMQTTPASS: string;
    FMQTTDEBUG: Integer;

    {Section: HTTP}
    FHTTPENABLED: Integer;
    FHTTPURL: string;
    FHTTPINTERVAL: Integer;

    {Section: WEB_SERVER}
    FWEB_SERVERenabled: Integer;
    FWEB_SERVERport: Integer;

    {Section: PROXY}
    FPROXYusar: Integer;
    FPROXYHOST: string;
    FPROXYPORT: Integer;
    FPROXYUSER: string;
    FPROXYPASS: string;
  public
    procedure LoadSettings(Ini: TMemIniFile);
    procedure SaveSettings(Ini: TMemIniFile);
    
    procedure LoadFromFile(const FileName: string);
    procedure SaveToFile(const FileName: string);

    {Section: CONFIGURACION}
    property CONFIGURACIONINI_PLAYER: string read FCONFIGURACIONINI_PLAYER write FCONFIGURACIONINI_PLAYER;
    property CONFIGURACIONGRUPO: Integer read FCONFIGURACIONGRUPO write FCONFIGURACIONGRUPO;
    property CONFIGURACIONreboot: Integer read FCONFIGURACIONreboot write FCONFIGURACIONreboot;
    property CONFIGURACIONintervalo_reboot: Integer read FCONFIGURACIONintervalo_reboot write FCONFIGURACIONintervalo_reboot;
    property CONFIGURACIONintervalo_idioma: Integer read FCONFIGURACIONintervalo_idioma write FCONFIGURACIONintervalo_idioma;
    property CONFIGURACIONtime_diff: Integer read FCONFIGURACIONtime_diff write FCONFIGURACIONtime_diff;

    {Section: MQTT}
    property MQTTENABLED: Integer read FMQTTENABLED write FMQTTENABLED;
    property MQTTHOST: string read FMQTTHOST write FMQTTHOST;
    property MQTTPORT: Integer read FMQTTPORT write FMQTTPORT;
    property MQTTSSL: Integer read FMQTTSSL write FMQTTSSL;
    property MQTTTOPIC_PUB: string read FMQTTTOPIC_PUB write FMQTTTOPIC_PUB;
    property MQTTTOPIC_SUB: string read FMQTTTOPIC_SUB write FMQTTTOPIC_SUB;
    property MQTTUSER: string read FMQTTUSER write FMQTTUSER;
    property MQTTPASS: string read FMQTTPASS write FMQTTPASS;
    property MQTTDEBUG: Integer read FMQTTDEBUG write FMQTTDEBUG;

    {Section: HTTP}
    property HTTPENABLED: Integer read FHTTPENABLED write FHTTPENABLED;
    property HTTPURL: string read FHTTPURL write FHTTPURL;
    property HTTPINTERVAL: Integer read FHTTPINTERVAL write FHTTPINTERVAL;

    {Section: WEB_SERVER}
    property WEB_SERVERenabled: Integer read FWEB_SERVERenabled write FWEB_SERVERenabled;
    property WEB_SERVERport: Integer read FWEB_SERVERport write FWEB_SERVERport;

    {Section: PROXY}
    property PROXYusar: Integer read FPROXYusar write FPROXYusar;
    property PROXYHOST: string read FPROXYHOST write FPROXYHOST;
    property PROXYPORT: Integer read FPROXYPORT write FPROXYPORT;
    property PROXYUSER: string read FPROXYUSER write FPROXYUSER;
    property PROXYPASS: string read FPROXYPASS write FPROXYPASS;
  end;

var
  IniOptions: TIniOptions = nil;

implementation

procedure TIniOptions.LoadSettings(Ini: TMemIniFile);
begin
  if Ini <> nil then
  begin
    {Section: CONFIGURACION}
    FCONFIGURACIONINI_PLAYER := Ini.ReadString(csIniCONFIGURACIONSection, csIniCONFIGURACIONINI_PLAYER, 'c:\hmsipplayer\hmsipplayer.ini');
    FCONFIGURACIONGRUPO := Ini.ReadInteger(csIniCONFIGURACIONSection, csIniCONFIGURACIONGRUPO, 432218);
    FCONFIGURACIONreboot := Ini.ReadInteger(csIniCONFIGURACIONSection, csIniCONFIGURACIONreboot, 1);
    FCONFIGURACIONintervalo_reboot := Ini.ReadInteger(csIniCONFIGURACIONSection, csIniCONFIGURACIONintervalo_reboot, 240);
    FCONFIGURACIONintervalo_idioma := Ini.ReadInteger(csIniCONFIGURACIONSection, csIniCONFIGURACIONintervalo_idioma, 30);
    FCONFIGURACIONtime_diff := Ini.ReadInteger(csIniCONFIGURACIONSection, csIniCONFIGURACIONtime_diff, 999);

    {Section: MQTT}
    FMQTTENABLED := Ini.ReadInteger(csIniMQTTSection, csIniMQTTENABLED, 0);
    FMQTTHOST := Ini.ReadString(csIniMQTTSection, csIniMQTTHOST, '192.168.1.102');
    FMQTTPORT := Ini.ReadInteger(csIniMQTTSection, csIniMQTTPORT, 1883);
    FMQTTSSL := Ini.ReadInteger(csIniMQTTSection, csIniMQTTSSL, 0);
    FMQTTTOPIC_PUB := Ini.ReadString(csIniMQTTSection, csIniMQTTTOPIC_PUB, 'spotdyna/46/inusual/17');
    FMQTTTOPIC_SUB := Ini.ReadString(csIniMQTTSection, csIniMQTTTOPIC_SUB, 'terminales/#');
    FMQTTUSER := Ini.ReadString(csIniMQTTSection, csIniMQTTUSER, '');
    FMQTTPASS := Ini.ReadString(csIniMQTTSection, csIniMQTTPASS, '');
    FMQTTDEBUG := Ini.ReadInteger(csIniMQTTSection, csIniMQTTDEBUG, 0);

    {Section: HTTP}
    FHTTPENABLED := Ini.ReadInteger(csIniHTTPSection, csIniHTTPENABLED, 1);
    FHTTPURL := Ini.ReadString(csIniHTTPSection, csIniHTTPURL, 'http://192.168.1.102/site_emergencias/ws_promo.cfm');
    FHTTPINTERVAL := Ini.ReadInteger(csIniHTTPSection, csIniHTTPINTERVAL, 5);

    {Section: WEB_SERVER}
    FWEB_SERVERenabled := Ini.ReadInteger(csIniWEB_SERVERSection, csIniWEB_SERVERenabled, 1);
    FWEB_SERVERport := Ini.ReadInteger(csIniWEB_SERVERSection, csIniWEB_SERVERport, 85);

    {Section: PROXY}
    FPROXYusar := Ini.ReadInteger(csIniPROXYSection, csIniPROXYusar, 0);
    FPROXYHOST := Ini.ReadString(csIniPROXYSection, csIniPROXYHOST, '127.0.0.1');
    FPROXYPORT := Ini.ReadInteger(csIniPROXYSection, csIniPROXYPORT, 80);
    FPROXYUSER := Ini.ReadString(csIniPROXYSection, csIniPROXYUSER, 'pepe');
    FPROXYPASS := Ini.ReadString(csIniPROXYSection, csIniPROXYPASS, 'pepe');
  end;
end;

procedure TIniOptions.SaveSettings(Ini: TMemIniFile);
begin
  if Ini <> nil then
  begin
    {Section: CONFIGURACION}
    Ini.WriteString(csIniCONFIGURACIONSection, csIniCONFIGURACIONINI_PLAYER, FCONFIGURACIONINI_PLAYER);
    Ini.WriteInteger(csIniCONFIGURACIONSection, csIniCONFIGURACIONGRUPO, FCONFIGURACIONGRUPO);
    Ini.WriteInteger(csIniCONFIGURACIONSection, csIniCONFIGURACIONreboot, FCONFIGURACIONreboot);
    Ini.WriteInteger(csIniCONFIGURACIONSection, csIniCONFIGURACIONintervalo_reboot, FCONFIGURACIONintervalo_reboot);
    Ini.WriteInteger(csIniCONFIGURACIONSection, csIniCONFIGURACIONintervalo_idioma, FCONFIGURACIONintervalo_idioma);
    Ini.WriteInteger(csIniCONFIGURACIONSection, csIniCONFIGURACIONtime_diff, FCONFIGURACIONtime_diff);

    {Section: MQTT}
    Ini.WriteInteger(csIniMQTTSection, csIniMQTTENABLED, FMQTTENABLED);
    Ini.WriteString(csIniMQTTSection, csIniMQTTHOST, FMQTTHOST);
    Ini.WriteInteger(csIniMQTTSection, csIniMQTTPORT, FMQTTPORT);
    Ini.WriteInteger(csIniMQTTSection, csIniMQTTSSL, FMQTTSSL);
    Ini.WriteString(csIniMQTTSection, csIniMQTTTOPIC_PUB, FMQTTTOPIC_PUB);
    Ini.WriteString(csIniMQTTSection, csIniMQTTTOPIC_SUB, FMQTTTOPIC_SUB);
    Ini.WriteString(csIniMQTTSection, csIniMQTTUSER, FMQTTUSER);
    Ini.WriteString(csIniMQTTSection, csIniMQTTPASS, FMQTTPASS);
    Ini.WriteInteger(csIniMQTTSection, csIniMQTTDEBUG, FMQTTDEBUG);

    {Section: HTTP}
    Ini.WriteInteger(csIniHTTPSection, csIniHTTPENABLED, FHTTPENABLED);
    Ini.WriteString(csIniHTTPSection, csIniHTTPURL, FHTTPURL);
    Ini.WriteInteger(csIniHTTPSection, csIniHTTPINTERVAL, FHTTPINTERVAL);

    {Section: WEB_SERVER}
    Ini.WriteInteger(csIniWEB_SERVERSection, csIniWEB_SERVERenabled, FWEB_SERVERenabled);
    Ini.WriteInteger(csIniWEB_SERVERSection, csIniWEB_SERVERport, FWEB_SERVERport);

    {Section: PROXY}
    Ini.WriteInteger(csIniPROXYSection, csIniPROXYusar, FPROXYusar);
    Ini.WriteString(csIniPROXYSection, csIniPROXYHOST, FPROXYHOST);
    Ini.WriteInteger(csIniPROXYSection, csIniPROXYPORT, FPROXYPORT);
    Ini.WriteString(csIniPROXYSection, csIniPROXYUSER, FPROXYUSER);
    Ini.WriteString(csIniPROXYSection, csIniPROXYPASS, FPROXYPASS);
  end;
end;

procedure TIniOptions.LoadFromFile(const FileName: string);
var
  Ini: TMemIniFile;
begin
  Ini := TMemIniFile.Create(FileName);
  try
    LoadSettings(Ini);
  finally
    Ini.Free;
  end;
end;

procedure TIniOptions.SaveToFile(const FileName: string);
var
  Ini: TMemIniFile;
begin
  Ini := TMemIniFile.Create(FileName);
  try
    SaveSettings(Ini);
    Ini.UpdateFile;
  finally
    Ini.Free;
  end;
end;

initialization
  IniOptions := TIniOptions.Create;

finalization
  IniOptions.Free;

end.

