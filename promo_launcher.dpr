program promo_launcher;

uses
  Vcl.Forms,
  u_main in 'u_main.pas' {main},
  Vcl.Themes,
  Vcl.Styles,
  leer_config_player in 'leer_config_player.pas',
  Winapi.Messages,
  Vcl.Graphics,
  Vcl.ComCtrls,
  Winapi.RichEdit,
  uAcciones in '..\HMSIP Server 9\Unidades Comunes\uAcciones.pas',
  leer_config in 'leer_config.pas';

type
  TRichEditStyleHookFix = class(TScrollingStyleHook)
  strict private
    procedure EMSetBkgndColor(var Message: TMessage); message EM_SETBKGNDCOLOR;
  end;

    { TRichEditStyleHookFix }

    procedure TRichEditStyleHookFix.EMSetBkgndColor(var Message: TMessage);
    begin
      Message.LParam := ColorToRGB(StyleServices.GetStyleColor(scEdit));
      Handled := False;
    end;

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Tablet Dark');
  TStyleManager.Engine.RegisterStyleHook(TRichEdit, TRichEditStyleHookFix);
  Application.CreateForm(Tmain, main);
  Application.Run;
end.
