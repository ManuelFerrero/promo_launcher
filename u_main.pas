unit u_main;

/////////////////////PROMO LAUNCHER ////////////////////////////////////
///  V1.0.1 Version inicial
///  V1.0.2 A�adida gestion de cambio de idioma
///  V1.0.3 A�adido parametro de diferencia de zona horaria en las llamadas al WS
///  V1.0.4 Se coge el time_diff de la config del player si no esta en el ini de la aplicacion.
///  V1.0.5 Corregido bug en la interpretacion de la lista de idiomas.

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, TMS.MQTT.Global, Vcl.StdCtrls,
  TMS.MQTT.Client,Xml.xmldom, Xml.XMLIntf, Xml.XMLDoc, System.RegularExpressions,
  system.JSON,TLHelp32, dcMinTray, Vcl.ComCtrls,StrUtils,IdURI,ShellApi,
  AdvOfficeImage,  Vcl.Styles, BMDThread, IdUDPServer, IdGlobal, IdSocketHandle,
  IdBaseComponent, IdComponent, IdUDPBase, TMS.MQTT.Logging, IdUDPClient,uAcciones,
  FormSize, clTcpClient, clTcpClientTls, clHttp, Vcl.ExtCtrls, clTcpServer,
  clTcpServerTls, clHttpRequest,clHttpHeader,clUtils,System.SyncObjs, system.DateUtils,
  Vcl.DdeMan, clSimpleHttpServer, IdCustomTCPServer, IdCustomHTTPServer,
  IdHTTPServer, IdContext;

const 
    _VALID_CHARSET         = ['0'..'9','A'..'Z','a'..'z','?','.','>','<','+','-','~','!','@','#','$','%','&','*','(',')','_','=','{','}','[',']','|','\','/',':',';',' ',','];


type

  TCharSet = set of Char;

  r_valor = record
     valor: string;
     etiqueta : string;
  end;
  arr_valor = Array of r_valor;

type
  Tmain = class(TForm)
    mqtt2: TTMSMQTTClient;
    mTTY: TRichEdit;
    AdvOfficeImage1: TAdvOfficeImage;
    lbl_ws_aw: TLabel;
    lbl_http: TLabel;
    thr_mqtt: TBMDThread;
    thr_http: TBMDThread;
    TMSMQTTLogger1: TTMSMQTTLogger;
    FormSize1: TFormSize;
    clHttp1: TclHttp;
    tmr_http: TTimer;
    clTcpServer1: TclTcpServer;
    clHttpRequest1: TclHttpRequest;
    lbl_promo_activa: TLabel;
    lbl_uptime: TLabel;
    lbl_idioma: TLabel;
    tmr_idioma: TTimer;
    Server1: TIdHTTPServer;
    procedure FormCreate(Sender: TObject);
    procedure mqtt2PublishReceived(ASender: TObject; APacketID: Word;
      ATopic: string; APayload: TArray<System.Byte>);
    procedure mqtt2ConnectedStatusChanged(ASender: TObject;
      const AConnected: Boolean; AStatus: TTMSMQTTConnectionStatus);
    procedure mqtt2SubscriptionAcknowledged(ASender: TObject; APacketID: Word;
      ASubscriptions: TTMSMQTTSubscriptions);
    procedure thr_mqttExecute(Sender: TObject; Thread: TBMDExecuteThread;
      var Data: Pointer);
    procedure TMSMQTTLogger1Log(Sender: TObject; ALevel: TTMSMQTTLogLevel;
      AMessage: string);
    procedure tmr_httpTimer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure mTTYChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure tmr_idiomaTimer(Sender: TObject);
    procedure Server1Status(ASender: TObject; const AStatus: TIdStatus;
      const AStatusText: string);
    procedure Server1CommandGet(AContext: TIdContext;
      ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
  private
    { Private declarations }
  public
    { Public declarations }
    modo_debug      : boolean;
    mqtt_packetid   ,
    error_broker    : integer;
    mqtt            : TTMSMQTTClient;
    FSynchronizer   : TCriticalSection;
    FIsStop         : Boolean;
    limpiar         : boolean; //Estra variabnle indicara cuando limpiar memoria despues de una promo;
    json_promo      : string;
    promo_activa    : integer;
    promo_recibida  : integer;
    Uptime          ,
    hora_arranque   : TDateTime;
    hora_uptime     : integer;
    index_idioma    : integer;
    diff_hora       : integer;
    list_idiomas: Tstringlist;

    procedure reiniciar_aplicacion;
    procedure matar_proceso(proceso: string);
    Procedure TTY(S : String; tipo_log:integer=1);
    procedure limpiar_logs;

    procedure PutLogMessage(const ALogMessage: string);

  end;

var
  main: Tmain;
  logerror  ,
  logaud    : TextFile;
  aSnapshotHandle     : THandle;
  aProcessEntry32     : TProcessEntry32;


implementation

uses
  leer_config, leer_config_player;

{$R *.dfm}


 ////////////////////////////////FUNCIONES AUXILIARES///////////////////////////////////////
function IsNumber(s: string):boolean;
var
  iValue, iCode: Integer;
begin
    val(s, iValue, iCode);
    if iCode = 0 then
      IsNumber:=True
    else
      IsNumber:=False;
end;

function isDate ( const DateString: string ): boolean;
var
   salida: TDateTime;
begin
  try
    result :=TryStrToDateTime ( DateString,salida );
  except
    result := false;
  end;
end;

function Occurrences(const Substring, Text: string): integer;
var
  offset: integer;
begin
  result := 0;
  offset := PosEx(Substring, Text, 1);
  while offset <> 0 do
  begin
    inc(result);
    offset := PosEx(Substring, Text, offset + length(Substring));
  end;
end;

function isvalidjson(json: string): Boolean;
var
    jo: TJSONObject;
begin
    jo:=nil;
    isvalidjson:=false;
    try
        jo := TJSONObject.ParseJSONValue(json) as TJSONObject;
        isvalidjson:=true;
    except
        on e:Exception do
            isvalidjson:=false;
    end;
end;

function limpiaporqueria(s: string): string;

begin
  s:=StringReplace(s,#$D,'',[rfreplaceall]);
  s:=StringReplace(s,#$A,'',[rfreplaceall]);
  s:=StringReplace(s,#9,'',[rfreplaceall]);
  limpiaporqueria:=s;
end;

function selectSingleNode(ADOMDocument: IDOMDocument; const nodePath: WideString): IDOMNode;
var
  LDomNodeSelect : IDomNodeSelect;
begin
  if not Assigned(ADOMDocument) or not Supports(ADOMDocument.documentElement, IDomNodeSelect, LDomNodeSelect) then
   Exit;
  Result:=LDomNodeSelect.selectNode(nodePath);
end;

function SelectNodes(ADOMDocument: IDOMDocument; const nodePath: WideString): IDOMNodeList;
var
  LDomNodeSelect : IDomNodeSelect;
begin
  if not Assigned(ADOMDocument) or not Supports(ADOMDocument.documentElement, IDomNodeSelect, LDomNodeSelect) then
   Exit;
  //or just LDomNodeSelect:= (ADOMDocument.documentElement as IDOMNodeSelect);
  Result:=LDomNodeSelect.selectNodes(nodePath);
end;

function GetURLFilename(const FilePath:String;Const Delimiter:String='/'):String;
    var I: Integer;
begin
    I := LastDelimiter(Delimiter, FILEPATH);
    Result := Copy(FILEPATH, I + 1, MaxInt);
    Result := TIdURI.UrlDecode(Result);
end;

function GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := Format('%d.%d.%d',
    [LongRec(FixedPtr.dwFileVersionMS).Hi,  //major
     LongRec(FixedPtr.dwFileVersionMS).Lo,  //minor
     LongRec(FixedPtr.dwFileVersionLS).Hi])  //release
     //LongRec(FixedPtr.dwFileVersionLS).Lo]) //build

   //Result:=_VERSION;
end;

function StripNonConforming(const S: string;
  const ValidChars: TCharSet): string; 
var 
  DestI: Integer; 
  SourceI: Integer; 
begin 

  SetLength(Result, Length(S));
  DestI := 0; 
  for SourceI := 1 to Length(S) do 
    if S[SourceI] in ValidChars then
    begin 
      Inc(DestI); 
      Result[DestI] := S[SourceI]
    end; 
  SetLength(Result, DestI)
end;

function findinarray(a: array of string; v: string):integer;
var
    n: integer;

begin 
    for n := 0 to Length(a)-1 do
        if a[n]=v then
            begin
                findinarray:=n+1;
                exit;
            end;
            
end;

procedure TrimAppMemorySize;
var
  MainHandle: THandle;
begin
  try
    MainHandle := OpenProcess(PROCESS_ALL_ACCESS, false, GetCurrentProcessID);
    SetProcessWorkingSetSize(MainHandle, $FFFFFFFF, $FFFFFFFF);
    CloseHandle(MainHandle);
  except
  end;
  Application.ProcessMessages;
end;


////////////////////////////////FIN FUNCIONES AUXILIARES///////////////////////////////////////
///
///  /////////////////////////// FUNCIONES COMUNES //////////////////////////////////////////////
///
procedure Tmain.reiniciar_aplicacion;
var
    comando     ,
    parametros  : string;
begin
    comando:='"'+application.exename+'"';
    parametros:='';
    ShellExecute(handle,'open',PWideChar(comando), PWideChar(parametros),nil,SW_SHOWNORMAL);

end;


procedure Tmain.Server1CommandGet(AContext: TIdContext;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
var
  path: string;
  page: string;
  statusCode: Integer;
  statusText: string;
  URI:   String;

begin
  if modo_debug then PutLogMessage('Request: ' + ARequestInfo.URI + ' ' + ARequestInfo.Document + ' Length: ' + IntToStr(ARequestInfo.ContentLength));
  AResponseInfo.ContentType := 'application/json';
  URI:=copy(ARequestInfo.URI,2,length(ARequestInfo.URI));
  if (isnumber(URI)) then
    begin
        promo_recibida:=strtoint(URI);
         //Con cada peticion se reinicia el contador de idioma
        tmr_idioma.Enabled:=false;
        tmr_idioma.Enabled:=true;
    end
    else promo_recibida:=0;
  lbl_http.Enabled:=true;
  page:=json_promo;
  statusCode := 200;
  statusText := 'OK';
  //Envio de la respuesta del webserver
  AResponseInfo.ContentText := page;
  Aresponseinfo.ResponseText:=statusText;
  AResponseInfo.ResponseNo := statusCode;

end;

procedure Tmain.Server1Status(ASender: TObject; const AStatus: TIdStatus;
  const AStatusText: string);
begin
  tty('HTTP Server :'+AStatusText);
end;

procedure Tmain.TMSMQTTLogger1Log(Sender: TObject; ALevel: TTMSMQTTLogLevel;
  AMessage: string);
begin
    if modo_debug then
    tty(AMessage);
end;

Procedure TMain.TTY(S : String; tipo_log:integer=1);
var
   nombrefic :  string;
begin
  try
       //enviar_mensaje_ws(s);
       if mTTY.lines.count > 100 then mTTY.Lines.Delete(0);
       case tipo_log of
            0: mTTY.SelAttributes.Color:=clsilver;   //Informativo
            1: mTTY.SelAttributes.Color:=clwhite;  //General
            2: mTTY.SelAttributes.Color:=clWebCyan;   //Proceso
            3: begin
                  mTTY.SelAttributes.Color:=clWebOrange;    //Error
                  mTTY.SelAttributes.Style:=mTTY.SelAttributes.Style+[fsbold];
               end;
            4: mTTY.SelAttributes.Color:=clYellow;  //Loguear en el fichero de log
       end;

       mTTY.lines.add(DateTimeToStr(now)+' - '+s);

       if (tipo_log=3) OR (tipo_log=4) then
        begin
            s:=stringreplace(s,Chr(13),' ',[rfReplaceAll]);
            s:=stringreplace(s,Chr(10),'',[rfReplaceAll]);
            nombrefic := extractfilepath(application.ExeName)+'\logs\LOG_promo_launcher'+formatdatetime('yyyymmdd',now)+'.LOG';
            //Escritura en el log de errores para la monitorizacion
            try
                AssignFile(logerror, nombrefic);
                if not FileExists(nombrefic) then
                    Rewrite(logerror)
                    else
                        Append(logerror);
                Writeln(logerror,FormatDateTime('dd/mm/yyyy hh:nn:ss',now)+' - '+s);
                CloseFile(logerror);
            except

            end;
            //Lo a�adimos en el memo para que envie el mail
        end;
  except
    on e:exception do
    begin
        mTTY.Lines.Add(FormatDateTime('dd/mm/yyyy hh:nn:ss',now)+' - ERROR EN TTY... '+e.Message);
    end;

  end;
end;

procedure TMain.limpiar_logs;
var
    sr: TSearchRec;

begin
    if (FindFirst(ExtractFilePath(Application.ExeName) + '\LOG\*.LOG', faAnyFile, sr) = 0) then
         begin
              repeat
                  if daysbetween(now,FileDateToDateTime(sr.Time)) > 30 then
                    deletefile(PChar(ExtractFilePath(Application.ExeName) +'\LOG\'+ sr.Name));
              until FindNext(sr) <> 0;
         end;
    System.SysUtils.FindClose(sr);
end;

////////////////////////////////FIN FUNCIONES COMUNES///////////////////////////////////////



//procedure Tmain.Server1ReceiveRequest(Sender: TObject;
//  AConnection: TclHttpUserConnection; const AMethod, AUri: string;
//  AHeader: TclHttpRequestHeader; ABody: TStream);
//var
//  path: string;
//  page: string;
//  statusCode: Integer;
//  statusText: string;
//  URI:   String;
//
//begin
//  if modo_debug then PutLogMessage('Request: ' + AMethod + ' ' + AUri + ' Length: ' + IntToStr(ABody.Size));
//  AConnection.ResponseHeader.ContentType := 'application/json';
//  URI:=copy(AUri,2,length(AUri));
//  if (isnumber(URI)) then
//    begin
//        promo_recibida:=strtoint(URI);
//         //Con cada peticion se reinicia el contador de idioma
//        tmr_idioma.Enabled:=false;
//        tmr_idioma.Enabled:=true;
//    end
//    else promo_recibida:=0;
//  lbl_http.Enabled:=true;
//  page:=json_promo;
//  statusCode := 200;
//  statusText := 'OK';
//  //Envio de la respuesta del webserver
//  Aconnection.ResponseHeader.Allow:='*';
//  Aconnection.ResponseHeader.extrafields.AddPair('Access-Control-Allow-Origin','*');
//  (Sender as TclHttpServer).SendResponse(AConnection, statusCode, statusText, page);
//
//  end;


procedure Tmain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    Server1.StopListening;
end;

procedure Tmain.FormCreate(Sender: TObject);
begin
    Matar_proceso(extractfilename(application.ExeName));
    TTY('Inicio de la aplicacion',4);
    modo_debug:=fileexists('c:\debug.txt');
    if modo_debug then tty('Modo debug activado',0);
    
    forcedirectories(extractfilepath(application.exename)+'\logs');

    limpiar_logs;
    self.Caption:='PROMO LAUNCHER V'+GetAppVersionStr;
    IniOptions.LoadFromFile(ChangeFileExt(Application.ExeName,'.ini'));
    tty('Leido ini '+ ChangeFileExt(Application.ExeName,'.ini'));
    diff_hora:=inioptions.CONFIGURACIONtime_diff;
    if fileexists(inioptions.CONFIGURACIONINI_PLAYER) then
      begin
          IniOptions_player.LoadFromFile(inioptions.CONFIGURACIONINI_PLAYER);
          tty('Leido ini PLAYER'+ inioptions.CONFIGURACIONINI_PLAYER);
          tty('ID Terminal '+inttostr(inioptions_player.CONFIGURACIONID));
          diff_hora:=inioptions_player.CONFIGURACIONZONAHORARIA;
      end;
    //EL time_diff de  l plicacion prima sobre la del player
    if inioptions.CONFIGURACIONtime_diff<>999 then
        diff_hora:=inioptions.CONFIGURACIONtime_diff;

    tty('Ajuste de zona horaria :'+inttostr(diff_hora),1);
    tmr_idioma.Interval:=inioptions.CONFIGURACIONintervalo_idioma*1000;

    if (inioptions.WEB_SERVERENABLED=1) then
      begin
          json_promo:='[]';
          promo_activa:=0;
          promo_recibida:=0;
          limpiar:=false;
          FSynchronizer := TCriticalSection.Create();
          server1.DefaultPort:=inioptions.WEB_SERVERport;
          server1.Active:=true;
          server1.StartListening;
          index_idioma:=0;
          list_idiomas:=TStringlist.Create;
          list_idiomas.commatext:='-';
      end;

    Uptime                      := Now;
    hora_arranque               := Now;

    if (inioptions.HTTPENABLED=1) then
        begin

            tty('HTTP client activo URL '+inioptions.HTTPURL+' intervalo:'+inttostr(inioptions.HTTPINTERVAL));
            lbl_ws_aw.Enabled:=true;
            if inioptions.PROXYusar=1 then
                begin
                    clHTTP1.ProxySettings.Server    :=inioptions.PROXYHOST;
                    clHTTP1.ProxySettings.Port      :=inioptions.PROXYPORT;
                    clHTTP1.ProxySettings.UserName  :=inioptions.PROXYUSER;
                    clHTTP1.ProxySettings.Password  :=inioptions.PROXYPASS;
                end;
            tmr_http.Interval:=inioptions.HTTPINTERVAL*1000;
            tmr_http.Enabled:=true;
        end;


    //Asignar los valores de la configuracion
    if inioptions.MQTTENABLED=1 then
        begin
            thr_mqtt.Start;
        end;

end;

procedure Tmain.PutLogMessage(const ALogMessage: string);
begin
  if not (csDestroying in ComponentState) then
  begin
    FSynchronizer.Enter();
    try
      mtty.Lines.Add(ALogMessage);
    finally
      FSynchronizer.Leave();
    end;
  end;
end;

procedure Tmain.thr_mqttExecute(Sender: TObject; Thread: TBMDExecuteThread;
  var Data: Pointer);

begin
    mqtt                            := TTMSMQTTClient.Create(Self);
    mqtt.OnPublishReceived          :=mqtt2PublishReceived;
    mqtt.OnSubscriptionAcknowledged :=mqtt2SubscriptionAcknowledged;
    mqtt.OnConnectedStatusChanged   :=mqtt2ConnectedStatusChanged;
    mqtt.BrokerHostName             :=inioptions.MQTTHOST;
    mqtt.BrokerPort                 :=inioptions.MQTTPORT;
    mqtt.UseSSL                     :=inioptions.MQTTSSL=1;
    mqtt.Connect(true);
end;

procedure Tmain.tmr_httpTimer(Sender: TObject);
var
   response: TStrings;
   respuesta: string;
   JSonValue: TJSonValue;
   JsonArray: TJSONArray;
   ArrayElement: TJSonValue;
   FoundValue: string;
   idiomas: string;


begin
    try
        tty('Comprobando HTTP',0);
        if modo_debug then PutLogMessage(inioptions.HTTPURL+'?store='+inttostr(inioptions.CONFIGURACIONGRUPO)+'&action=check'+'&diff='+inttostr(diff_hora));
        response:=TStringList.Create();
        clHttp1.Close;
        clHttp1.Get(inioptions.HTTPURL+'?store='+inttostr(inioptions.CONFIGURACIONGRUPO)+'&action=check'+'&diff='+inttostr(diff_hora),response);
        hora_uptime:=minutesBetween(hora_arranque, now);
        lbl_uptime.Caption:=IntToStr(DaysBetween(now, hora_arranque))+'d '+inttostr(HoursBetween(hora_arranque, now) mod 24)+'h '+inttostr(MinutesBetween(now, hora_arranque) mod 60)+'m';
       // if modo_debug then tty(response.text);
        respuesta:=limpiaporqueria(response.Text);
        if respuesta='[]' then      //No hay promos
            begin
                if limpiar then
                    begin
                        TrimAppMemorySize;  //Ha finalizado una promo y limpiamos porqueria en la memoria
                        limpiar:=false;
                        json_promo:='[]';
                        list_idiomas.commatext:='-';
                        index_idioma:=0;
                    end;
            end else
                begin
                    limpiar:=true;
                    if isvalidjson('{'+respuesta+'}') then
                        begin
                            json_promo:=respuesta;
                            JsonValue := TJSonObject.ParseJSONValue(respuesta);
                            JsonArray := JsonValue as TJSONArray;
                            for ArrayElement in JsonArray do begin
                               if ArrayElement.GetValue<string>('cod_promo')=inttostr(promo_activa) then
                                   begin

                                       idiomas:=ArrayElement.ToString;
                                       idiomas:=copy(idiomas,pos('list_idiomas',idiomas)+15,length(idiomas));
                                       idiomas:=copy(idiomas,1,Pos(']',idiomas)-1);
                                       idiomas:=stringreplace(idiomas,'"','',[rfReplaceAll]);
                                       list_idiomas.CommaText:=idiomas;
                                       lbl_idioma.caption:=list_idiomas[index_idioma];
                                       TJSonObject(ArrayElement).AddPair('idioma',list_idiomas[index_idioma]);
                                       tty(arrayelement.ToString);
                                   end;

                             end;
                             json_promo:=JsonArray.ToString;
                        end else
                            tty('Respuesta de WS servidor '+respuesta+' no valida',3);

                      //tty(json_promo);

                end;
        lbl_http.Enabled:=false;

        //Si ha cambiado el ID de promo, mandamos para que se marque como en ejecucion
        if promo_activa<>promo_recibida then
            begin
                if promo_recibida=0 then
                    begin
                        tty('Finalizada promo activa :'+inttostr(promo_activa),4);
                        clHttp1.Close;
                        clHttp1.Get(inioptions.HTTPURL+'?store='+inttostr(inioptions.CONFIGURACIONGRUPO)+'&action=status&status=F&id='+inttostr(promo_activa),response);
                        if modo_debug then tty('store='+inttostr(inioptions.CONFIGURACIONGRUPO)+'&action=status&status=F&id='+inttostr(promo_recibida),0);
                        if modo_debug then tty(response.Text,0);
                        list_idiomas.commatext:='-';
                        index_idioma:=0;
                        lbl_idioma.caption:=list_idiomas[index_idioma];
                        tmr_idioma.Enabled:=false;

                        if (inioptions.CONFIGURACIONreboot=1) and  (hora_uptime>inioptions.CONFIGURACIONintervalo_reboot) then
                            begin
                                tty('Reinicio diario programado',4);
                                reiniciar_aplicacion;
                            end;
                    end else
                        begin
                             tty('Recibida nueva promo activa :'+inttostr(promo_recibida),4);
                             clHttp1.Close;
                             clHttp1.Get(inioptions.HTTPURL+'?store='+inttostr(inioptions.CONFIGURACIONGRUPO)+'&action=playing&id='+inttostr(promo_recibida),response);
                             if modo_debug then tty('store='+inttostr(inioptions.CONFIGURACIONGRUPO)+'&action=playing&id='+inttostr(promo_recibida),0);
                             if modo_debug then tty(response.Text,0);
                        end;
                 promo_activa:=promo_recibida;
                 lbl_promo_activa.caption:=inttostr(promo_activa);


            end;
    except
        on e:exception do
            begin
                tty('Error HTTP: '+e.Message,3);
            end;

    end;
    response.Free;
end;



procedure Tmain.tmr_idiomaTimer(Sender: TObject);
begin
//Cuando se cumplen 30 segundos sin haber recibido ninguna peticion, se cambia el idioma
       if index_idioma<(list_idiomas.Count-1) then
            inc(index_idioma)
       else
            index_idioma:=0;
       tmr_idioma.Enabled:=false;
       lbl_idioma.caption:=list_idiomas[index_idioma];
       tty('cambio de idioma '+inttostr(index_idioma));
end;

procedure Tmain.FormDestroy(Sender: TObject);
begin

    //FSynchronizer.Free();
end;

procedure Tmain.matar_proceso(proceso: string);
var
  Ret       ,
  bContinue : BOOL;
  PrID      ,
  PrID_este : Integer; // processidentifier
  Ph        : THandle;   // processhandle
begin
     try
        aSnapshotHandle        := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
        aProcessEntry32.dwSize := SizeOf(aProcessEntry32);
        bContinue              := Process32First(aSnapshotHandle, aProcessEntry32);
        while Integer(bContinue) <> 0 do
        begin
                PrID:=StrToInt('$' + IntToHex(aProcessEntry32.th32ProcessID, 4));
                if aProcessEntry32.szExeFile=proceso then
                    begin
                        Ph := OpenProcess(1, BOOL(0), PrID);
                        PrID_este:=GetCurrentProcessId;
                        if PrID_este<>PrID then
                            begin
                                Ret := TerminateProcess(Ph, 0);
                                if Integer(Ret) = 0
                                   then tty('No se ha podico matar ' + proceso,3)
                                   else tty('Proceso ' + proceso + ' matado');
                            end;
                    end;
                bContinue := Process32Next(aSnapshotHandle, aProcessEntry32);
         end;
         CloseHandle(aSnapshotHandle);
     Except
       on e:Exception do
          begin
            tty('Error Cerrando procesos previos: ' + e.message,3);
          end;

     end;
end;

procedure Tmain.mqtt2ConnectedStatusChanged(ASender: TObject;
  const AConnected: Boolean; AStatus: TTMSMQTTConnectionStatus);
begin
    if (AConnected) then
     begin
         // The client is now connected and you can now start interacting with the broker.
         //Ahora suscribimos el cliente MQTT al topic

        try
          tty('Contectado con broker '+TTMSMQTTClient(ASender).BrokerHostName);
          TTMSMQTTClient(ASender).Subscribe(inioptions.MQTTTOPIC_SUB+'/'+inttostr(inioptions_player.CONFIGURACIONID));
          error_broker:=0;
          //Ya nos hemos suscrito, asi que reseteamos el contador de errores
        except
          on e:exception do
              begin
                  tty('Error al suscribir al topic MQTT '+e.Message);
                  Inc(error_broker);
                  error_broker:=0;
              end;
        end;
     end
     else
     begin
     // The client is NOT connected and any interaction with the broker will result in an exception.
     case AStatus of
         csConnectionRejected_InvalidProtocolVersion,
         csConnectionRejected_InvalidIdentifier,
         csConnectionRejected_ServerUnavailable:
         begin
            TTY(TControl(ASender).name+': Perdida conexion con broker MQTT->connection rejected',3); // the connection with the broker is lost
            Inc(error_broker);
          end;
         csConnectionRejected_InvalidCredentials,
         csConnectionRejected_ClientNotAuthorized:
            begin
                tty(TControl(ASender).name+': Conexion rechazada por el broker',3);
                Inc(error_broker);
            end;
         csConnectionLost:
          begin
                tty(TControl(ASender).name+': Perdida conexion con broker MQTT->connectionLost',3); // the connection with the broker is lost
                Inc(error_broker);
          end;
         csReconnecting:
         tty('Intentando reconectar con broker MQTT'); // The client is trying to reconnect to the broker
     end;
     end;

     //Si despues de n reintentos de conexion al broker no lo conseguimos, reiniciamos server
     if (error_broker>=25) then
        begin
            //error_broker:=0;
            tty('Reinicio de la aplicacion por fallo reiterado de la conexion MQTT',4);
            reiniciar_aplicacion;
        end;
end;

procedure Tmain.mqtt2PublishReceived(ASender: TObject; APacketID: Word;
  ATopic: string; APayload: TArray<System.Byte>);
var
    strmensaje      : AnsiString;
    jobj            : Tjsonobject;
    jv              : TJSONValue;
    mensaje         : string;


begin
    //Data := ABinding.PeerIP + '#' + BytesToString(aData,0,254);
    //Procesado de ordenes y comandos
    strmensaje:=TEncoding.UTF8.GetString(APayload);
    IF inioptions.MQTTDEBUG=1 THEN tty(TTMSMQTTClient(ASender).Name+' '+strmensaje,0);
    try


    except
        on e:exception do
            begin
                tty('Error procesando mensaje MQTT '+e.Message+#13+strmensaje);
            end;
    end;
end;

procedure Tmain.mqtt2SubscriptionAcknowledged(ASender: TObject; APacketID: Word;
  ASubscriptions: TTMSMQTTSubscriptions);
begin   
    if ASubscriptions[0].Accepted then
    begin
        tty(Tcontrol(ASender).name+' Suscrito al topic MQTT '+ASubscriptions[0].topic);
        error_broker:=0;
    end;

    if ASubscriptions[1].Accepted then
    begin
        tty(Tcontrol(ASender).name+' Suscrito al topic MQTT '+ASubscriptions[1].topic);
        error_broker:=0;
    end;
    
end;

procedure Tmain.mTTYChange(Sender: TObject);
begin
    SendMessage(mtty.handle, WM_VSCROLL, SB_BOTTOM, 0);
end;

end.
